import React, { Component } from "react";
import {
  Text,
  View,
  WebView,
  Platform,
  StyleSheet,
  StatusBar,
  ScrolActlView,
  ActivityIndicator,
  Image
} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import ScalableText from "react-native-text";
let WebViewRef;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showError: false,
      isVisible: true,
      showWeb: true
    };
  }

  reloadPage() {
    //this.setState({showError:false});
    WebViewRef.reload();
  }
  renderLoader() {
    return (
      <ActivityIndicator
        style={{ top: 100, alignSelf: "center" }}
        animating={true}
        size="large"
        color={"#04b2e2"}
      />
    );
  }

  renderError() {
    return (
      <View style={{ alignSelf: "center", top: 100 }}>
        <MaterialIcons
          name="error-outline"
          size={50}
          style={{ alignSelf: "center" }}
          color="#e74c3c"
        />
        <ScalableText style={{ color: "#34495e", fontSize: 30 }}>
          Sorry
        </ScalableText>

        <ScalableText style={{ color: "#7f8c8d" }}>
          Could not load the page you are looking for.
        </ScalableText>
        <MaterialIcons
          name="refresh"
          size={50}
          style={{ alignSelf: "center", top: 17 }}
          onPress={() => WebViewRef.reload()}
          color="#1abc9c"
        />
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <WebView
          ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
          source={{ uri: "http://reports.northamericanpos.com/Login/Index" }}
          //onLoadStart={()=>this.setState({showWeb:false})}
          //onLoadEnd={()=>this.setState({isVisible:false,showWeb:true})}
          startInLoadingState={true}
          automaticallyAdjustContentInsets={true}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          scalesPageToFit={true}
          renderError={this.renderError}
          renderLoading={this.renderLoader}
          style={{ flex: 1 }}
          //style={{marginTop: (Platform.OS === 'ios') ? 20 : 0}}
          onError={() => this.setState({ showError: true, isVisible: false })}
        />
      </View>
    );
  }
}
